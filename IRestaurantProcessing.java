import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

public interface IRestaurantProcessing {
    public void createItem(String[] name,double[] price);
    public abstract void deleteItem(String[] name,int ok);
    public abstract void editItem(String[]  newname,double[] newprice);
    public void createOrder(ArrayList<MenuItem> m, Order o);
    public abstract double computePrice();
    public abstract void generateBill(Order order) throws IOException;
}

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ChefController {
    private Chef view;
    public  ChefController(Chef view){
        this.view=view;
        this.view.AddListenerOrders(new ActionOrder());
        this.view.AddListenerBack(new ActionBack());
    }
    class ActionOrder implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            view.order();
        }
    }
    class ActionBack implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            view.back();
        }
    }
}

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AdminController {
    private Administrator view;
    public AdminController(Administrator view){
        this.view=view;
        this.view.AddListenerCreate(new ActionCreate());
        this.view.AddListenerBase(new ActionBase());
        this.view.AddListenerAdd(new ActionADD());
        this.view.AddListenerComp(new ActionComp());
        this.view.AddListenerGo(new ActionGo());
        this.view.AddListenerAddC(new ActionADDC());
        this.view.AddlistenerDelete(new ActionDelete());
        this.view.AddListenerBaseD(new ActionBaseD());
        this.view.AddListenerCompD(new ActionCompD());
        this.view.AddListenerAddBD(new ActionBaseDel());
        this.view.AddListenerAddCD(new ActionCompDel());
        this.view.AddlistenerEdit(new ActionEdit());
        this.view.AddlistenerEditC(new ActionEditC());
        this.view.AddlistenerEditCo(new ActionEditCo());
        this.view.AddlistenerEditCoN(new ActionEditCoN());
        this.view.AddlistenerBack(new ActionBack());
        this.view.AddlistenerMenu(new ActionMenu());
    }
    class ActionCreate implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            view.create();
        }
    }
    class ActionDelete implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            view.delete();
        }
    }
    class ActionBase implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            view.basep();
        }
    }
    class ActionADD implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
             view.add();
        }
    }
    class ActionComp implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            view.compp();
        }
    }
    class ActionGo implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            view.printcom();
        }
    }

    class ActionADDC implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            view.addcom();
        }

    }
    class ActionBaseD implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            view.basep1();
        }
    }
    class ActionCompD implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            view.compp1();
        }
    }
    class ActionBaseDel implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            view.delete1();
        }
    }
    class ActionCompDel implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            view.deletecom();
        }
    }
    class ActionEdit implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            view.edit();
        }
    }
    class ActionEditC implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            view.ed();
        }
    }
    class ActionEditCo implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            view.edit1();
        }
    }
    class ActionEditCoN implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            view.edd();
        }
    }
    class ActionBack implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            view.back();
        }
    }
    class ActionMenu  implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            view.menu();
        }
    }
}

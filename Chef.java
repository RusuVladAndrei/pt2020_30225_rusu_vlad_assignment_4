import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;

public class Chef implements Observer {
    private Restaurant r;
    private JFrame frame;
    private JPanel st=new JPanel();
    private JPanel orders=new JPanel();
    private JButton or=new JButton("Orders");
    private JButton back=new JButton("Back");

    public Restaurant getR() {
        return r;
    }

    public void setR(Restaurant r) {
        this.r = r;
    }
    public void back(){
        frame.getContentPane().removeAll();
        orders.removeAll();
        st.removeAll();
        st.add(or);
        frame.repaint();
        frame.setSize(500,500);
        or.setVisible(true);
        orders.setVisible(false);
        st.setVisible(true);
        frame.add(st);
        frame.add(orders);
        frame.setVisible(true);
    }
    public Chef(){
        frame=new JFrame("Chef");
        frame.setLayout(new FlowLayout());
        frame.setVisible(true);
        frame.setSize(500,500);
        st.add(or);
        //st.add(back);
        or.setVisible(true);
        back.setVisible(false);
        frame.add(st);
        frame.add(orders);
        st.setVisible(true);
        orders.setVisible(false);

        //frame.pack();
    }
    public void order(){
        st.setVisible(false);
        orders.add(back);
        orders.setVisible(true);
        back.setVisible(true);
        Observable o=new Observable();
        this.update(o,this.r.map);
    }
    @Override
    public void update(Observable o, Object arg) {
        HashMap<Order,ArrayList<MenuItem>> hash= (HashMap<Order, ArrayList<MenuItem>>) arg;
        String str="Orders with composite products:";
        CompositeProduct c=new CompositeProduct();
        //MenuItem m=new CompositeProduct();
        for (Order or:
                hash.keySet()) {
            ArrayList<MenuItem> value=hash.get(or);
            int ok=0;
            for (MenuItem m:
                 value) {
                for (MenuItem mm:
                     this.r.menu) {
                    if(mm.getName().equals(m.getName())){
                        if(mm.getClass()==c.getClass()){
                            if(value.size()==1){
                                str+="Order with id:" + or.getId() ;
                                ok=1;
                                break;
                            }
                            else{
                                str+="Order with id:" + or.getId() + ",";
                                ok=1;
                                break;
                            }
                        }
                    }
                }
                if(ok==1){
                    break;
                }
            }
        }
        JLabel j=new JLabel(str);
        orders.add(j);
    }
    public void AddListenerOrders(ActionListener a){
        or.addActionListener(a);
    }
    public void AddListenerBack(ActionListener a){
        back.addActionListener(a);
    }
}

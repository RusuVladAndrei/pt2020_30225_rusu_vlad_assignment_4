public class MenuItem extends Restaurant{
    private String name;
    private double price;

    public void add(MenuItem item){

    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public double computePrice() {
         return this.price;
    }
}

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Observable;

public class Restaurant extends Observable implements IRestaurantProcessing {
    HashMap<Order,ArrayList<MenuItem>> map=new HashMap<>();
    ArrayList<MenuItem> menu=new ArrayList<>();
    @Override
    public void createItem(String[] name,double[] price){

    }

    @Override
    public void deleteItem(String[] name,int ok) {

    }

    @Override
    public void editItem(String[]  newname,double[] newprice) {

    }

    @Override
    public void createOrder(ArrayList<MenuItem> m, Order o){

    }

    @Override
    public double computePrice() {
        return 0;
    }

    @Override
    public void generateBill(Order order) {

    }

    public static void main(String[] args) {
        Administrator a=new Administrator();
        Restaurant r=new Restaurant();
        a.setR(r);
        AdminController ac=new AdminController(a);
        r=a.getR();
        Waiter w=new Waiter();
        w.setR(r);
        WaiterController con=new WaiterController(w);
        Chef c=new Chef();
        r=w.getR();
        c.setR(r);
        ChefController cc=new ChefController(c);
    }
}

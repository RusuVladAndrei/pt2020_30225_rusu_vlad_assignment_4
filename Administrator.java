import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.ListIterator;
import java.util.PrimitiveIterator;

public class Administrator implements IRestaurantProcessing{
    private Restaurant r=new Restaurant();

    public Restaurant getR() {
        return r;
    }

    public void setR(Restaurant r) {
        this.r = r;
    }

    private JFrame frame;
    private JButton cI=new JButton("Create Item");;
    private JButton dI=new JButton("Delete Item");;
    private JButton eI=new JButton("Edit Item");;
    private JButton base=new JButton("Base Product");
    private JButton comp=new JButton("Composite Product");
    private JButton base1=new JButton("Base Product");
    private JButton comp1=new JButton("Composite Product");
    private JButton cr=new JButton("Add");
    private JButton cr1=new JButton("Add");
    private JButton cr2=new JButton("Delete");
    private JButton cr3=new JButton("Delete");
    private JButton cr4=new JButton("Edit");
    private JButton cr5=new JButton("Edit");
    private JPanel st=new JPanel();
    private JPanel create=new JPanel(new FlowLayout());
    private JPanel delete=new JPanel();
    private JPanel edit=new JPanel();
    private JPanel bs=new JPanel();
    private JPanel bs1=new JPanel();
    private JPanel cs=new JPanel();
    private JPanel cs1=new JPanel();
    private JPanel cs2=new JPanel();
    private JPanel cs3=new JPanel();
    private JPanel addcomp=new JPanel();
    private JPanel addcomp1=new JPanel();
    private JPanel men=new JPanel();
    private JButton numb=new JButton("Go");
    private JButton numb1=new JButton("Go");
    private JButton numb3=new JButton("Go");
    private JButton numb4=new JButton("Go");
    private JButton back=new JButton("Back");
    private JButton menu=new JButton("Menu");
    private int max=1;
    private JTextField[] basename;
    private JTextField[] baseprice;
    private JLabel label=new JLabel("Name/Price");
    private JLabel label2=new JLabel("Name ComProduct");
    private JLabel label3=new JLabel("New Name ComProduct");

    private JLabel label1=new JLabel("Number of products");
    private JTextField num=new JTextField(5);
    private JTextField num1=new JTextField(5);
    private JTextField nume1=new JTextField(5);
    private JTextField nume2=new JTextField(5);
    public JPanel getDelete() {
        return delete;
    }

    public Administrator(){
        frame=new JFrame("Administrator");
        frame.setLayout(new FlowLayout());
        frame.setSize(800,200);
        frame.setVisible(true);
        create.setSize(500,500);
        delete.setSize(500,500);
        edit.setSize(500,500);
        create.add(base);
        create.add(comp);
        delete.add(base1);
        delete.add(comp1);
        edit.add(label2);
        edit.add(nume1);
        edit.add(numb3);
        st.setSize(500,500);
        st.add(cI);
        st.add(dI);
        st.add(eI);
        st.add(menu);
        create.add(back);
        delete.add(back);
        edit.add(back);
        frame.add(st);
        frame.add(create);
        frame.add(delete);
        frame.add(edit);
        frame.add(back);
        frame.add(men);
        back.setVisible(false);
        menu.setVisible(true);
        st.setVisible(true);
        create.setVisible(false);
        delete.setVisible(false);
        edit.setVisible(false);
        bs.setVisible(false);
        men.setVisible(false);
    }
    public void menu(){
        st.setVisible(false);
        back.setVisible(true);
        create.setVisible(false);
        bs.setVisible(false);
        cs.setVisible(false);
        bs1.setVisible(false);
        cs1.setVisible(false);
        addcomp.setVisible(false);
        cs2.setVisible(false);
        cs3.setVisible(false);
        delete.setVisible(false);
        edit.setVisible(false);
        JLabel[] j=new JLabel[this.r.menu.size()];
        for(int i=0;i<this.r.menu.size();i++){
            j[i]=new JLabel(this.r.menu.get(i).getName()+ " ");
        }
        for(int i=0;i<this.r.menu.size();i++){
            men.add(j[i]);
        }
        men.add(back);
        men.setVisible(true);
    }
    public void create(){
        st.setVisible(false);
        back.setVisible(true);
        create.setVisible(true);
        bs.setVisible(false);
        cs.setVisible(false);
        bs1.setVisible(false);
        cs1.setVisible(false);
        addcomp.setVisible(false);
        cs2.setVisible(false);
        cs3.setVisible(false);
        delete.setVisible(false);
        edit.setVisible(false);
        men.setVisible(false);
    }
    public void delete(){
        st.setVisible(false);
        back.setVisible(true);
        delete.setVisible(true);
        create.setVisible(false);
        bs.setVisible(false);
        cs.setVisible(false);
        bs1.setVisible(false);
        cs1.setVisible(false);
        addcomp.setVisible(false);
        cs2.setVisible(false);
        cs3.setVisible(false);
        edit.setVisible(false);
        men.setVisible(false);
    }
    public void edit(){
        st.setVisible(false);
        back.setVisible(true);
        edit.setVisible(true);
        create.setVisible(false);
        bs.setVisible(false);
        cs.setVisible(false);
        bs1.setVisible(false);
        cs1.setVisible(false);
        addcomp.setVisible(false);
        cs2.setVisible(false);
        cs3.setVisible(false);
        delete.setVisible(false);
        men.setVisible(false);
    }
    public void back(){
        frame.getContentPane().removeAll();
        create.removeAll();
        delete.removeAll();
        edit.removeAll();
        bs.removeAll();
        bs1.removeAll();
        cs.removeAll();
        cs1.removeAll();
        cs2.removeAll();
        cs3.removeAll();
        men.removeAll();
        addcomp.removeAll();
        num.setText("");
        nume1.setText("");
        nume2.setText("");
        frame.repaint();
       // st.setVisible(true);
        frame.setSize(800,200);
        frame.setVisible(true);
        create.setSize(500,500);
        delete.setSize(500,500);
        edit.setSize(500,500);
        create.add(base);
        create.add(comp);
        delete.add(base1);
        delete.add(comp1);
        edit.add(label2);
        edit.add(nume1);
        edit.add(numb3);
        st.setSize(500,500);
        st.add(cI);
        st.add(dI);
        st.add(eI);
        st.add(menu);
        create.add(back);
        delete.add(back);
        edit.add(back);
        frame.add(st);
        frame.add(create);
        frame.add(delete);
        frame.add(edit);
        frame.add(back);
        frame.add(men);
        back.setVisible(false);
        st.setVisible(true);
        create.setVisible(false);
        delete.setVisible(false);
        edit.setVisible(false);
        bs.setVisible(false);
        men.setVisible(false);
    }
    public void basep(){
        basename=new JTextField[1];
        baseprice=new JTextField[1];
        basename[0]=new JTextField(5);
        baseprice[0]=new JTextField(5);
        bs.add(label);
        bs.add(cr);
        bs.add(basename[0]);
        bs.add(baseprice[0]);
        bs.add(back);
        frame.add(bs);
        back.setVisible(true);
        create.setVisible(false);
        bs.setVisible(true);
        create.setVisible(false);
        cs.setVisible(false);
        bs1.setVisible(false);
        cs1.setVisible(false);
        addcomp.setVisible(false);
        cs2.setVisible(false);
        cs3.setVisible(false);
        delete.setVisible(false);
        edit.setVisible(false);
        men.setVisible(false);
    }
    public void basep1(){
        basename=new JTextField[1];
        baseprice=new JTextField[1];
        basename[0]=new JTextField(5);
        baseprice[0]=new JTextField(5);
        JLabel l=new JLabel("Nume");
        bs1.add(l);
        bs1.add(cr2);
        bs1.add(basename[0]);
        bs1.add(back);
        back.setVisible(true);
        frame.add(bs1);
        delete.setVisible(false);
        bs1.setVisible(true);
        bs.setVisible(false);
        cs.setVisible(false);
        cs1.setVisible(false);
        addcomp.setVisible(false);
        cs2.setVisible(false);
        cs3.setVisible(false);
        delete.setVisible(false);
        edit.setVisible(false);
        men.setVisible(false);
    }
    public void compp(){
        create.setVisible(false);
        cs.add(label1);
        cs.add(num);
        cs.add(numb);
        cs.add(label2);
        cs.add(nume1);
        cs.add(back);
        back.setVisible(true);
        cs.setVisible(true);
        bs.setVisible(false);
        bs1.setVisible(false);
        cs1.setVisible(false);
        addcomp.setVisible(false);
        cs2.setVisible(false);
        cs3.setVisible(false);
        delete.setVisible(false);
        edit.setVisible(false);
        men.setVisible(false);
        frame.add(cs);
    }
    public void compp1(){
        delete.setVisible(false);
        cs1.add(label2);
        cs1.add(nume1);
        cs1.add(cr3);
        cs1.add(back);
        back.setVisible(true);
        cs1.setVisible(true);
        bs.setVisible(false);
        cs.setVisible(false);
        bs1.setVisible(false);
        addcomp.setVisible(false);
        cs2.setVisible(false);
        cs3.setVisible(false);
        delete.setVisible(false);
        edit.setVisible(false);
        men.setVisible(false);
        frame.add(cs1);
    }
    public void printcom(){
        cs.setVisible(false);
        max=Integer.valueOf(num.getText());
        JLabel[] l=new JLabel[max];
        basename=new JTextField[max];
        baseprice=new JTextField[max];
        for(int i=0;i<max;i++){
            l[i]=new JLabel(label.getText());
            basename[i]=new JTextField(5);
            baseprice[i]=new JTextField(5);
        }
        for(int i=0;i<max;i++){
            addcomp.add(l[i]);
            addcomp.add(basename[i]);
            addcomp.add(baseprice[i]);
        }
        addcomp.add(cr1);
        addcomp.add(back);
        back.setVisible(true);
        addcomp.setVisible(true);
        bs.setVisible(false);
        cs.setVisible(false);
        bs1.setVisible(false);
        cs1.setVisible(false);
        cs2.setVisible(false);
        cs3.setVisible(false);
        delete.setVisible(false);
        edit.setVisible(false);
        men.setVisible(false);
        frame.add(addcomp);
    }

    public void add(){
        String[] str=new String[1];
        double[] pr=new double[1];
        str[0]=basename[0].getText();
        pr[0]=Double.valueOf(baseprice[0].getText());
        this.createItem(str,pr);
    }
    public void delete1(){
        String[] str=new String[1];
        double[] pr=new double[1];
        str[0]=basename[0].getText();
        this.deleteItem(str,0);
    }
    public void addcom(){
        String[] str=new String[max];
        double[] pr=new double[max];
        for(int i=0;i<max;i++){
            str[i]=basename[i].getText();
            pr[i]=Double.valueOf(baseprice[i].getText());
        }
        this.createItem(str,pr);
    }
    public void deletecom(){
        String[] str=new String[max];
        double[] pr=new double[max];
        str[0]=nume1.getText();
        this.deleteItem(str,1);
    }
    public void ed(){
        edit.setVisible(false);
        CompositeProduct m1=new CompositeProduct();
        for (MenuItem m:
             this.r.menu) {
            if(m.getName().equals(nume1.getText())){
                if(m.getClass()==m1.getClass()){
                    CompositeProduct m2= (CompositeProduct) m;
                    max=m2.getCom().size();
                }
                else{
                    max=1;
                }
            }
        }
        JLabel[] l=new JLabel[max];
        basename=new JTextField[max];
        baseprice=new JTextField[max];
        for(int i=0;i<max;i++){
            l[i]=new JLabel(label.getText());
            baseprice[i]=new JTextField(5);
            basename[i]=new JTextField(5);
        }
        for(int i=0;i<max;i++){
            cs2.add(l[i]);
            cs2.add(basename[i]);
            cs2.add(baseprice[i]);
        }
        if(max==1){
            cs2.add(cr4);
            cs2.setVisible(true);
            cs2.add(back);
            back.setVisible(true);
            bs.setVisible(false);
            cs.setVisible(false);
            bs1.setVisible(false);
            cs1.setVisible(false);
            addcomp.setVisible(false);
            cs3.setVisible(false);
            delete.setVisible(false);
            edit.setVisible(false);
            men.setVisible(false);
            frame.add(cs2);
            return;
        }
        cs2.add(numb4);
        cs2.add(back);
        back.setVisible(true);
        cs2.setVisible(true);
        bs.setVisible(false);
        cs.setVisible(false);
        bs1.setVisible(false);
        cs1.setVisible(false);
        addcomp.setVisible(false);
        cs3.setVisible(false);
        delete.setVisible(false);
        edit.setVisible(false);
        men.setVisible(false);
        frame.add(cs2);
    }
    public void edd(){
        cs2.setVisible(false);
        cs3.add(label3);
        cs3.add(nume2);
        cs3.add(cr4);
        cs3.add(back);
        back.setVisible(true);
        cs3.setVisible(true);
        bs.setVisible(false);
        cs.setVisible(false);
        bs1.setVisible(false);
        cs1.setVisible(false);
        addcomp.setVisible(false);
        cs2.setVisible(false);
        delete.setVisible(false);
        edit.setVisible(false);
        men.setVisible(false);
        frame.add(cs3);
    }
    public void edit1(){
        String[] str=new String[max];
        double[] pr=new double[max];
        for(int i=0;i<max;i++){
            str[i]=basename[i].getText();
            pr[i]=Double.valueOf(baseprice[i].getText());
        }
        this.editItem(str,pr);
    }
    @Override
    public void createItem(String[] name,double[] price) {
         if(name.length!=1){
             MenuItem m=new CompositeProduct();
             m.setName(nume1.getText());

             for(int i=0;i<name.length;i++){
                 MenuItem b=new BaseProduct();
                 b.setPrice(price[i]);b.setName(name[i]);
                 m.add(b);
             }
             m.setPrice(m.computePrice());
             this.r.menu.add(m);
            // System.out.println(m.getClass());
         }
         else if(name.length==1){
             MenuItem b=new BaseProduct();
             b.setName(name[0]);b.setPrice(price[0]);
             this.r.menu.add(b);
         }

    }

    @Override
    public void deleteItem(String[] name,int ok) {
        if(ok==1){
            MenuItem m=new CompositeProduct();
            m.setName(nume1.getText());
            for(int i=0;i<name.length;i++){
                MenuItem b=new BaseProduct();
                b.setName(name[i]);
                m.add(b);
            }
            for (ListIterator<MenuItem> i = this.r.menu.listIterator(); i.hasNext();) {
                MenuItem m1 = i.next();
                if(m1.getName().equals(m.getName())){
                    i.remove();
                }
            }
        }
        else if(ok==0){
            MenuItem b=new BaseProduct();
            b.setName(name[0]);
            for (ListIterator<MenuItem> i = this.r.menu.listIterator(); i.hasNext();) {
                MenuItem m = i.next();
                if (m.getName().equals(b.getName())) {
                    i.remove();
                }
            }

        }
    }

    @Override
    public void editItem(String[]  newname,double[] newprice) {
        if(newname.length!=1){
            MenuItem m=new CompositeProduct();
            m.setName(nume1.getText());
            for(int i=0;i<newname.length;i++){
                MenuItem b=new BaseProduct();
                b.setPrice(newprice[i]);b.setName(newname[i]);
                m.add(b);
            }
            m.setPrice(m.computePrice());
            int j=0;
            for(int i=0;i<this.r.menu.size();i++){
                if(this.r.menu.get(i).getName().equals(m.getName())){
                    j=i;break;
                }
            }
            m.setName(nume2.getText());
            this.r.menu.remove(this.r.menu.get(j));
            this.r.menu.add(m);
        }
        else if(newname.length==1){
            MenuItem b=new BaseProduct();
            b.setName(newname[0]);b.setPrice(newprice[0]);
            int j=0;
            for(int i=0;i<this.r.menu.size();i++){
                if(this.r.menu.get(i).getName().equals(nume1.getText())){
                    j=i;
                }
            }
            this.r.menu.remove(this.r.menu.get(j));
            this.r.menu.add(b);
        }
    }
    public void AddListenerCreate(ActionListener a){
        cI.addActionListener(a);
    }
    public void AddListenerBase(ActionListener a){
        base.addActionListener(a);
    }
    public void AddListenerBaseD(ActionListener a){
        base1.addActionListener(a);
    }
    public void AddListenerAdd(ActionListener a){
        cr.addActionListener(a);
    }
    public void AddListenerComp(ActionListener a){
        comp.addActionListener(a);
    }
    public void AddListenerCompD(ActionListener a){
        comp1.addActionListener(a);
    }
    public void AddListenerGo(ActionListener a){
        numb.addActionListener(a);
    }
    public void AddListenerAddC(ActionListener a){
        cr1.addActionListener(a);
    }
    public void AddListenerAddBD(ActionListener a){
        cr2.addActionListener(a);
    }
    public void AddListenerAddCD(ActionListener a){
        cr3.addActionListener(a);
    }
    public void AddlistenerDelete(ActionListener a){
        dI.addActionListener(a);
    }
    public void AddlistenerEdit(ActionListener a){
        eI.addActionListener(a);
    }
    public void AddlistenerEditC(ActionListener a){
        numb3.addActionListener(a);
    }
    public void AddlistenerEditCo(ActionListener a){
        cr4.addActionListener(a);
    }
    public void AddlistenerEditCoN(ActionListener a){
        numb4.addActionListener(a);
    }
    public void AddlistenerBack(ActionListener a){
        back.addActionListener(a);
    }
    public void AddlistenerMenu(ActionListener a){
        menu.addActionListener(a);
    }@Override
    public void createOrder(ArrayList<MenuItem> m, Order o)  {

    }

    @Override
    public double computePrice() {
        return 0;
    }

    @Override
    public void generateBill(Order order) {

    }
}

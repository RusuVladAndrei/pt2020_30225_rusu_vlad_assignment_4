import java.lang.reflect.Array;
import java.util.ArrayList;

public class CompositeProduct extends MenuItem{
    private ArrayList<MenuItem> com=new ArrayList<>();

    public ArrayList<MenuItem> getCom() {
        return com;
    }

    private int size;
    public void add(MenuItem item){
        this.com.add(item);
    }
    @Override
    public double computePrice() {
        float totalprice=0;
        for (MenuItem m:com) {
            totalprice+=m.computePrice();
        }
        return totalprice;
    }
}

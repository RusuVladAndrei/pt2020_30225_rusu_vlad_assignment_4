import com.sun.org.apache.xpath.internal.operations.Or;
import com.sun.xml.internal.messaging.saaj.soap.JpegDataContentHandler;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;

public class Waiter implements IRestaurantProcessing{
    private Restaurant r;
    private FileWriter[] f;
    private JFrame frame;
    private JButton co=new JButton("Create Order");;
    private JButton gb=new JButton("Generate Bill");;
    private JButton back=new JButton("Back");;
    private JButton go=new JButton("Go");;
    private JButton go1=new JButton("Generate");;
    private JButton ad=new JButton("Add");;
    private JButton or=new JButton("Orders");
    private JPanel st=new JPanel();
    private JPanel crorder=new JPanel();
    private JPanel gbill=new JPanel();
    private JPanel add=new JPanel();
    private JPanel orders=new JPanel();
    private int max=1;
    private JTextField[] basename;
    private JTextField[] baseprice;
    private JLabel label=new JLabel("OrderId/OrderDate/OrderTable/No of products");
    private JLabel labell=new JLabel("OrderId/OrderDate/OrderTable/");
    private JLabel label2=new JLabel("Name ComProduct");
    private JLabel label3=new JLabel("New Name ComProduct");

    private JLabel label1=new JLabel("Number of products");
    private JTextField id=new JTextField(5);
    private JTextField date=new JTextField(5);
    private JTextField table=new JTextField(5);
    private JTextField no=new JTextField(5);
    private JTextField[] t;

    public Restaurant getR() {
        return r;
    }

    public void setR(Restaurant r) {
        this.r = r;
    }
    public void orders(){
        st.setVisible(false);
        crorder.setVisible(false);
        gbill.setVisible(false);
        JLabel[] l=new JLabel[this.r.map.size()];
        for (Order o:
                this.r.map.keySet()) {
            String ll="";
            ArrayList<MenuItem> value=this.r.map.get(o);
            for (MenuItem m:
                 value) {
                //System.out.println(m.getClass());
                if(m!=value.get(value.size()-1))
                   ll+=m.getName()+ ",";
                else{
                    ll+=m.getName();
                }
            }
            ll+="\n";
            l[o.getId()-1]=new JLabel("Order" + o.getId() + ":"+ ll);
            orders.add(l[o.getId()-1]);
        }
        orders.add(back);
        orders.setVisible(true);
        frame.add(orders);
    }
    public void back(){
        frame.getContentPane().removeAll();
        st.removeAll();
        crorder.removeAll();
        gbill.removeAll();
        add.removeAll();
        orders.removeAll();
        frame.repaint();
        id.setText("");
        date.setText("");
        table.setText("");
        no.setText("");
        frame.setSize(600,300);
        frame.setVisible(true);
        st.add(co);
        st.add(gb);
        st.add(or);
        frame.add(st);
        frame.add(crorder);
        frame.add(gbill);
        st.setVisible(true);
        crorder.setVisible(false);
        gbill.setVisible(false);
        add.setVisible(false);
    }
    public Waiter(){
        f=new FileWriter[10];
        frame=new JFrame("Waiter");
        frame.setVisible(true);
        frame.setSize(600,300);
        st.add(co);
        st.add(gb);
        st.add(or);
        co.setVisible(true);
        gb.setVisible(true);
        crorder.add(back);
        gbill.add(back);
        frame.add(st);
        st.setVisible(true);
        crorder.setVisible(false);
        gbill.setVisible(false);
        add.setVisible(false);
    }
    public void crorder(){
        st.setVisible(false);
        crorder.add(label);
        crorder.add(id);
        crorder.add(date);
        crorder.add(table);
        crorder.add(no);
        crorder.add(go);
        crorder.add(back);
        crorder.setVisible(true);
        gbill.setVisible(false);
        add.setVisible(false);
        frame.add(crorder);
    }
    public void go(){
        crorder.setVisible(false);
        int max=Integer.valueOf(no.getText());
        JLabel[] l=new JLabel[max];
        t=new JTextField[max];
        for(int i=0;i<max;i++){
            l[i]=new JLabel("Nume Prod");
            t[i]=new JTextField(5);
        }
        for(int i=0;i<max;i++){
            add.add(l[i]);
            add.add(t[i]);
        }
        add.add(ad);
        add.add(back);
        add.setVisible(true);
        gbill.setVisible(false);
        st.setVisible(false);
        frame.add(add);
    }
    public void add(){
        Order o=new Order();
        o.setId(Integer.valueOf(id.getText()));
        o.setDate(LocalDate.parse(date.getText()));
        o.setTable(Integer.valueOf(table.getText()));
        ArrayList<MenuItem> or=new ArrayList();
        for (int i=0;i<t.length;i++){
            MenuItem m=new MenuItem();
            m.setName(t[i].getText());
            for (MenuItem mm:
                 this.r.menu) {
                if(m.getName().equals(mm.getName())){
                    m.setPrice(mm.computePrice());
                }
            }
            or.add(m);
        }
        this.createOrder(or,o);
    }
    public void gb(){
        st.setVisible(false);
        gbill.add(labell);
        gbill.add(id);
        gbill.add(date);
        gbill.add(table);
        gbill.add(go1);
        gbill.add(back);
        crorder.setVisible(false);
        add.setVisible(false);
        gbill.setVisible(true);
        frame.add(gbill);
    }
    public void gen() throws IOException {
        Order o=new Order();
        o.setId(Integer.valueOf(id.getText()));
        o.setDate(LocalDate.parse(date.getText()));
        o.setTable(Integer.valueOf(table.getText()));
        this.generateBill(o);
    }
    @Override
    public void createItem(String[] name,double[] price){

    }

    @Override
    public void deleteItem(String[] name,int ok) {

    }

    @Override
    public void editItem(String[]  newname,double[] newprice) {

    }

    @Override
    public void createOrder(ArrayList<MenuItem> m, Order o) {
        this.r.map.put(o,m);
    }

    @Override
    public double computePrice() {
        return 0;
    }

    @Override
    public void generateBill(Order order) throws IOException {
        float total=0;
        for (Order o:
             this.r.map.keySet()) {
             if(o.getId()==order.getId()){
                 ArrayList<MenuItem> value=this.r.map.get(o);
                 for (MenuItem m:
                      value) {
                     total+=m.computePrice();
                 }
             }
        }
        f[order.getId()-1]=new FileWriter("Order" + order.getId() + ".txt");
        f[order.getId()-1].write("Order:" + order.getId() + ",Price:" + total);
        f[order.getId()-1].close();
        //System.out.println(total);
    }
    public void AddListenerCreateOr(ActionListener a){
        co.addActionListener(a);
    }
    public void AddListenerGo(ActionListener a){
        go.addActionListener(a);
    }
    public void AddListenerAdd(ActionListener a){
        ad.addActionListener(a);
    }
    public void AddListenerBack(ActionListener a){
        back.addActionListener(a);
    }
    public void AddListenerGb(ActionListener a){
        gb.addActionListener(a);
    }
    public void AddListenerGen(ActionListener a){
        go1.addActionListener(a);
    }
    public void AddListenerOr(ActionListener a){
        or.addActionListener(a);
    }
}

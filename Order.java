import java.time.LocalDate;
import java.util.Date;
import java.util.Locale;

public class Order {
    private int id;
    private LocalDate date;
    private int table;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public int getTable() {
        return table;
    }

    public void setTable(int table) {
        this.table = table;
    }
    @Override
    public int hashCode(){
        return this.getId();
    }
    public Order hash(int value){
        Order o=new Order();
        o.setId(value);
        return o;
    }
}

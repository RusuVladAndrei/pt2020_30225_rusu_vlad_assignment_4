import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class WaiterController {
    private Waiter view;
    public WaiterController(Waiter view){
        this.view=view;
        this.view.AddListenerCreateOr(new ActionCreateOr());
        this.view.AddListenerGo(new ActionGo());
        this.view.AddListenerAdd(new ActionAdd());
        this.view.AddListenerBack(new ActionBack());
        this.view.AddListenerGb(new ActionGb());
        this.view.AddListenerGen(new ActionGen());
        this.view.AddListenerOr(new ActionOr());
    }
    class ActionCreateOr implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            view.crorder();
        }
    }
    class ActionGo implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            view.go();
        }
    }
    class ActionAdd implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            view.add();
        }
    }
    class ActionBack implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            view.back();
        }
    }
    class ActionGb implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            view.gb();
        }
    }
    class ActionGen implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                view.gen();
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }
    }
    class ActionOr implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            view.orders();
        }
    }
}
